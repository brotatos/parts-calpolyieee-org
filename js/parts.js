"use strict";

var parts = {};

parts.modules = {};

parts.setup = function () {
    var self = this;

    $.extend({
        put: function (url, data, callback, type) {
            return utils._ajaxRequest(url, data, callback, type, "PUT");
        },
        delete_: function (url, data, callback, type) {
            return utils._ajaxRequest(url, data, callback, type, "DELETE");
        }
    });

    $.ajaxSetup({xhrFields: {withCredentials: true}});

    $(document).ajaxError(function () {
        utils.danger(
            "There was an error processing your request. " +
                "Please reload the page and try again. "
        );
    });

    this.$login = $("#login");

    this.staticUrl = "http://static.calpolyieee.org";
    this.url = window.__env.API_URL;

    $.get(this.url + "/current_user/", function (data) {
        if (data.success) {
            self.user = data.user;
            self.launch();
        } else {
            self.$login.find(".panel-body").removeClass("loading");
            self.loginForm = new utils.Form({
                email: utils.Form.gen.input("Email", "lana.kane@isis.gov"),
                password: utils.Form.gen.password("Password", "••••••••")
            }, {
                url: self.url + "/users/login/",
                successAction: self.launch.bind(self)
            });
            self.$login.find(".panel-body").append(self.loginForm.render());
        }
    });
};

parts.launch = function () {
    var self = this, module;

    $("html").removeClass("loginPage");
    $("#logout").click(function () {
        $.get(self.url + "/users/logout/", function () {
            location.reload();
        });
    });

    this.loadKits = $.get(parts.url + "/kits/", utils.orAlert(function (data) {
        self.kits = data.kits;
    }));

    for (module in this.modules) {
        if (this.modules.hasOwnProperty(module)) {
            this.modules[module].setup();

            if (window.location.hash.substr(1) === module) {
                $("nav a[href=#" + module + "]").click();
            }
        }
    }

    $("nav a").click(function () {
        window.location.hash = this.getAttribute("href").substr(1);
    });
};
