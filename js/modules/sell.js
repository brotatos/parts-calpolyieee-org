"use strict";

parts.modules.sell = {};

parts.modules.sell.setup = function () {
    var self = this;

    this.$frame = $("#sell");
    this.allKits = {};
    this.allDiscounts = {};
    this.items = [];
    this.currPart = null; //just to mark it off

    ///finding part by id///
    this.$partNo = $("#sell > .half:first-child > .well:nth-child(1)");
    this.$partNoIn = this.$partNo.find("input");
    this.$partNoBtn = this.$partNo.children("a");

    ///manually entering part price///
    this.$partManual = $("#sell > .half:first-child > .well:nth-child(2)");
    this.$partManualIn = this.$partManual.find("input");
    this.$partManualBtn = this.$partManual.children("a");

    ///selecting a kit///
    this.$kit = $("#sell > .half:first-child > .well:nth-child(3)");
    this.$kitSel = this.$kit.find("select");
    this.$kitBtn = this.$kit.children("a");

    ///selecting a discount///
    this.$discount = $("#sell > .half:first-child > .well:nth-child(4)");
    this.$discountSel = this.$discount.find("select");
    this.$discountBtn = this.$discount.children("a");

    ///showing the part order///
    this.$orderBox = $("#sell > .half:nth-child(2) tbody");
    this.$continueBtn = $("#sell > #continueBtn");

    ///finalizing the order///
    this.$finalizeBox = $("#sell > div.well:last-child");
    this.$finalItems = this.$finalizeBox.children(".items");
    this.$cancelBtn = this.$finalizeBox.children("a:first-of-type");
    this.$purchaseBtn = this.$finalizeBox.children("a:last-of-type");
    this.$receiptTotal = this.$finalizeBox.children(".total");
    this.receiptForm = new utils.Form({
        purchaser: utils.Form.gen.input("Customer Name"),
        comment: utils.Form.gen.textarea("Receipt Comment", 2)
    }, {});

    this.$finalizeBox.children("h3:first-child").after(this.receiptForm.render());

    ///attach "error" and "clear" functions to part no lookup input element///
    this.$partNoIn.error = function () {
        var $span = $("<span class=\"glyphicon " +
                      "glyphicon-remove form-control-feedback\">");

        self.$partNoIn.parent().addClass("form-group has-feedback has-error");
        self.$partNoIn.parent().children("span").remove();
        self.$partNoIn.parent().append($span);
    };

    this.$partNoIn.clear = function ($elt) {
        self.$partNoIn.parent().children("span.glyphicon").remove();
        self.$partNoIn.parent()[0].className = "half";
    };

    ///the guts of actually creating a receipt///
    parts.loadKits.done(function () {
        self.$kitSel.empty().append(parts.kits.map(function (kit) {
            self.allKits[kit.name] = kit;
            return "<option>" + kit.name + "</option>";
        }));
    });

    $.get(parts.url + "/discounts/", utils.orAlert(function (data) {
        self.$discountSel.empty().append(data.discounts.map(function (dct) {
            self.allDiscounts[dct.name] = dct;
            return "<option>" + dct.name + "</option>";
        }));
    }));

    this.$continueBtn.click(this.makeReceipt.bind(this));
    this.$purchaseBtn.click(this.makePurchase.bind(this));
    this.$cancelBtn.click(function () {
        self.$frame.children().toggleClass("hidden");
        self.show();
    });

    this.$partNoBtn.click(function () {
        var url = parts.url + "/parts/" +
            parseInt(self.$partNoIn.val() || 0, 10);

        $.get(url, function (data) {
            if (data.success && self.items.map(function (part) {
                return part.skel.id;
            }).indexOf(data.part.id) === -1) {
                self.$partNoIn.clear();
                self.items.push(new parts.Part(data.part));
            } else {
                self.$partNoIn.error();
            }

            self.show();
        });
    });

    this.$partNoIn.keyup(function (event) {
        if (event.which === 13) {
            self.$partNoBtn.click();
        }
    });

    this.$partManualBtn.click(function () {
        self.items.push(new parts.Part({
            id: 0,
            category: "MANUAL",
            price: parseInt(
                parseFloat(self.$partManualIn.val() || 0) * 100, 10
            ),
            main_info: {name: "N/A"},
            addtl_info: []
        }));

        self.show();
    });

    this.$partManualIn.keyup(function (event) {
        if (event.which === 13) {
            self.$partManualBtn.click();
        }
    });

    this.$kitBtn.click(function () {
        var kit = self.allKits[self.$kitSel.val()];

        self.items.push(new parts.Part({
            category: "KIT",
            price: kit.price,
            id: kit.id + 20000,
            main_info: {name: kit.name},
            addtl_info: []
        }));

        self.show();
    });

    this.$discountBtn.click(function () {
        var discount = self.allDiscounts[self.$discountSel.val()];

        self.items.push(new parts.Part({
            category: "DISCOUNT",
            id: discount.id + 30000,
            main_info: {name: discount.name},
            addtl_info: []
        }));

        self.show();
    });
    
};

parts.modules.sell.makeReceipt = function () {
    var self = this, partMap = function (part) {
        return (new parts.Part(part)).renderToFinal();
    };

    this.$continueBtn.html("<img class=\"loading\" src=\"img/" +
                           "loading_blue.gif\"></a>");
    $.post(parts.url + "/receipts/finalize/", JSON.stringify({
        items: self.items.map(function (item) { return item.skel; })
    }), utils.orAlert(function (data) {
        self.$frame.children().toggleClass("hidden");
        self.receipt = data.receipt;
        self.$finalItems.empty().append(self.receipt.items.map(partMap));
        self.$receiptTotal
            .text("TOTAL: " + utils.toMoney(self.receipt.total_price));
    })).done(function () { self.$continueBtn.html("Continue &gt;"); });
};

parts.modules.sell.makePurchase = function () {
    var modal = new utils.ModalAlert(), $ics = $("<ul>"), $body = $("<div>");
    var url = parts.url + "/receipts/";

    $("body").append(modal.render({
        title: "Receipt Submission",
        body: $body,
        cancel: "OK"
    }));
    $body.append("<h4>Submitting...</h4>");
    modal.show();

    this.receipt.items.filter(function (part) {
        return part.category === "IC";
    }).forEach(function (part) {
        var $a = $("<a href=\"javascript: void 0;\">");

        $a.click(function () {
            window.open("/parts/" + part.id + "/location");
        }).text((new parts.Part(part)).toString());
        $ics.append($("<li>").append($a));
    });
    this.receipt.purchaser = this.receiptForm.skel.purchaser.val();
    this.receipt.comment = this.receiptForm.skel.comment.val();

    $.post(url, JSON.stringify(this.receipt), utils.orAlert(function (data) {
        var $pdf = utils.btn("Generate PDF", "danger").attr(
            "href",
            parts.url + "/receipts/pdf/" + data.receipt_id + ".pdf"
        );

        utils.info({msg: "Receipt submitted successfully. " +
                   "Click 'OK' to reload the page."}, $body.empty());
        $body.append($pdf, "<br>View Part Locations: ", $ics);
        modal.$cancel.unbind().click(function () { location.reload(); });
    }, $body));

};

parts.modules.sell.show = function () {
    var self = this;

    this.$orderBox.children().detach();

    this.items.forEach(function (part) {
        self.$orderBox.append(part.renderToLineItem());
        self.addRemoveListener(part);
    });
};

parts.modules.sell.addRemoveListener = function (part) {
    var self = this;

    part.$liRemove.unbind().click(function () {
        self.items.splice(self.items.indexOf(part), 1);
        self.show();
    });
};
