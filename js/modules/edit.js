"use strict";

parts.modules.edit = {};

parts.modules.edit.setup = function () {
    var self = this;

    this.$frame = $("#edit");
    this.$buttons = this.$frame.children(".buttons");
    this.$kitBuilder = this.$frame.children("#kitBuilder");
    this.$partBuilder = this.$frame.children("#partBuilder");

    ///the edit "menu"///
    this.$menu = this.$frame.children(".menu");
    this.$title = this.$menu.children("span:last-of-type");
    this.$returnLnk = this.$menu.children("a:first-of-type");
    this.$resetLnk = this.$menu.children("a:last-of-type");

    ///editing or creating a part///
    this.$partNoIn = this.$buttons.find("input");
    this.$editBtn = this.$buttons.find("div.input-group-btn > a:first-child");
    this.$newPartBtn = this.$buttons.find("ul li:first-child");

    ///selecting a kit///
    this.$kitTable = this.$buttons.find("tbody");
    this.$newKitBtn = this.$buttons.find(".panel .panel-heading a.btn");

    this.$newPartBtn.click(this.loadBuilder.bind(this, "part", null));
    this.$newKitBtn.click(this.loadBuilder.bind(this, "kit", null));

    parts.loadKits.done(function () {
        self.$kitTable.html(parts.kits.map(function (kit) {
            return utils.toTr(
                kit.id,
                kit.name,
                kit.stock,
                utils.btn("Edit").click(function () {
                    self.loadBuilder("kit", kit);
                })
            );
        }));
    });

    this.$editBtn.click(function () {
        var id = parseInt(self.$partNoIn.val(), 10) || 0;

        $.get(parts.url + "/parts/" + id, utils.orAlert(function (data) {
            self.loadBuilder("part", data.part);
        }));
    });
    
    this.$partNoIn.keyup(function () {
        if (event.which === 13) {
            self.$editBtn.click();
        }
    });

    this.$returnLnk.click(function () {
        self.$buttons
            .add(self.$menu)
            .add(self["$" + self.currBuilder + "Builder"])
            .toggleClass("hidden");
    });

    this.$resetLnk.click(function () {
        parts.modules[self.currBuilder + "_builder"].reset();
    });
};

parts.modules.edit.loadBuilder = function (type, skel) {
    var self = this;

    this.$title.text(type.charAt(0).toUpperCase() + type.slice(1) +
                     " Builder");
    
    this.currBuilder = type;
    this.$buttons
        .add(this.$menu)
        .add(this["$" + type + "Builder"])
        .toggleClass("hidden");

    if (skel) {
        parts.modules[type + "_builder"].populate(skel);
    } else {
        parts.modules[type + "_builder"].reset();
    }
};
