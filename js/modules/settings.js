"use strict";

parts.modules.settings = {};

parts.modules.settings.setup = function () {
    var self = this;

    $.get(parts.url + "/debug/", utils.orAlert(function (data) {
        if (data.debug) {
            $("nav > ul > li:last-child span.badge").removeClass("hidden");
        }
    }));

    ///info panel setup///
    this.$infoPanel = $("div#infoPanel");
    this.$name = this.$infoPanel.find("tr:nth-child(1) > td:nth-child(2)");
    this.$pass = this.$infoPanel.find("tr:nth-child(2) > td:nth-child(2)");
    this.$changePassBtn = this.$infoPanel.find("tr:nth-child(2) a.btn");
    this.changePassForm = new utils.Form({
        current: utils.Form.gen.password("Current Password"),
        "new": utils.Form.gen.password("New Password"),
        confirm: utils.Form.gen.password("Confirm Password")
    }, {
        url: parts.url + "/current_user/change_password/"
    });

    this.$changePassBtn.click(function () {
        var $form = self.changePassForm.render().prop("id", "changePassword");

        utils.quickModal("Change Password", $form);
    });

    $.get(parts.url + "/current_user/", utils.orAlert(function (data) {
        self.$name.text(data.user.name);
    }));
};
