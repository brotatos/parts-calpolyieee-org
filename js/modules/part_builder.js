"use strict";

parts.modules.part_builder = {};

parts.modules.part_builder.setup = function () {
    var i, field, self = this, url = parts.url + "/parts/";
    var locns = ["Choose...", "A1", "A2", "A3", "A4", "A5", "A6", "B1", "B2"];

    this.promises = [];

    ///setup///
    this.$frame = $("#edit > #partBuilder");
    this.addtlPairs = [];

    ///ic location input///
    this.$locnWrapper = $("<div class=\"locn\">");
    this.$locn1 = utils.Form.gen.select();
    this.$locn2 = $("<select class=\"form-control\">");
    this.$locn3 = this.$locn1.clone();
    this.$locn4 = utils.Form.gen.input(null, "required if \"OTHER\" selected");

    this.$locnWrapper.append(
        utils.Form._wrap("Location", this.$locn1[0]),
        utils.Form._wrap("Addtl Locn Info", this.$locn4[0])
    );
    this.$locn1.after(this.$locn2, this.$locn3);

    ///ic description input///
    this.$icAddtlWrapper = $("<div>");
    this.$icAddtlInfo = utils.Form.gen.textarea(null, 4);

    this.$icAddtlWrapper.append(
        utils.Form._wrap("Description", this.$icAddtlInfo[0])
    );

    ///the ic form///
    this.icForm = new utils.Form({
        category: utils.Form.gen.p("Category", "IC"),
        price: utils.Form.gen.money("Price"),
        name: utils.Form.gen.input("Name", "required"),
        location: utils.Form.gen.custom(
            this.$locnWrapper,
            this._locnVal.bind(this),
            this._locnReset.bind(this)
        ),
        stock: utils.Form.gen.input("Stock", "required"),
        part_no: utils.Form.gen.input("Part No.", "required"),
        mftr: utils.Form.gen.select("Manufacturer"),
        chip_type: utils.Form.gen.select("Chip Type"),
        addtl_info: utils.Form.gen.custom(
            this.$icAddtlWrapper,
            this._icAddtlVal.bind(this),
            this._icAddtlReset.bind(this)
        )
    }, {
        url: parts.url + "/parts/",
        title: "New IC",
        preSubmit: function (json) {
            json.main_info = {};
            json.main_info.name = json.part_no;
            json.category = "IC";
        }
    });

    this.$frame.append(this.icForm.render());

    this.$locn1.append(
        locns.map(function (locn) { return "<option>" + locn + "</option>"; }),
        "<option value=\"O\">OTHER</option>"
    );

    for (i = 0; i < 9; i++) {
        this.$locn2[0].innerHTML += "<option value=" + i + ">Row " +
            i + "</option>";
    }

    for (i = 0; i < 9; i++) {
        this.$locn3[0].innerHTML += "<option value=" + i + ">Col " +
            i + "</option>";
    }

    this.promises.push(
        $.get(parts.url + "/manufacturers/", utils.orAlert(function (data) {
            self.icForm.skel.mftr.fill(data.manufacturers, true);
        }))
    );

    this.promises.push(
        $.get(parts.url + "/chip_types/", utils.orAlert(function (data) {
            self.icForm.skel.chip_type.fill(data.chip_types, true);
        }))
    );

    ///static parts main_info field///
    this.$miWrapper = $("<div>");
    this.$miLabel = utils.Form.gen.p(null, "&nbsp;");
    this.$miValue = utils.Form.gen.input();
    this.$miName = utils.Form.gen.input(null, "required");

    this.$miWrapper.append(
        utils.Form._wrap("Main Property", this.$miLabel[0]),
        utils.Form._wrap("Value", this.$miValue[0]),
        utils.Form._wrap("Name", this.$miName[0])
    );

    ///static parts addtl_info field///
    this.$partAddtlWrapper = $("<div>");
    this.$partAddtlLabel = utils.Form.gen.p("&nbsp;");
    this.$newPairBtn = utils.btn("Add New Property");
    this.$delPairBtn = utils.btn("Remove", "danger").addClass("pull-right");

    this.$partAddtlWrapper.append(
        utils.Form._wrap("Addtl Props", this.$partAddtlLabel[0]),
        this.$newPairBtn
    );

    this.$newPairBtn.click(this.newPair.bind(this));
    this.$delPairBtn.click(this.delPair.bind(this));

    ///static parts///
    this.partForm = new utils.Form({
        category: utils.Form.gen.select("Category"),
        price: utils.Form.gen.money("Price"),
        main_info: utils.Form.gen.custom(
            this.$miWrapper,
            this._miVal.bind(this),
            this._miReset.bind(this)
        ),
        addtl_info: utils.Form.gen.custom(
            this.$partAddtlWrapper,
            this._partAddtlVal.bind(this),
            this._partAddtlReset.bind(this)
        )
    }, {
        url: parts.url + "/parts/",
        title: "New Static Part"
    });

    this.$frame.append(this.partForm.render());

    this.promises.push(
        $.get(parts.url + "/parts/category/names/", utils.orAlert(function (data) {
            self.partForm.skel.category.fill(data.cats.filter(function (cat) {
                return cat !== "IC";
            }), true);
        }))
    );

    ///final setup///
    this.icForm.skel.part_no.val("none");
};

parts.modules.part_builder.newPair = function (pair) {
    var self = this;
    var $value = utils.Form.gen.input();
    var $name = utils.Form.gen.input(null, "required");

    pair = pair || {name: ""};

    if (pair.name) {
        $name.val(pair.name);
    }

    if (pair.value) {
        $value.val(pair.value);
    }

    this.$newPairBtn.before(utils.Form._wrap("Value", $value[0]));
    this.$newPairBtn.before(utils.Form._wrap("Name", $name[0]));
    this.addtlPairs.push({$value: $value, $name: $name});

    if (this.addtlPairs.length) {
        this.$newPairBtn.after(this.$delPairBtn);
    }
};

parts.modules.part_builder.delPair = function () {
    this.addtlPairs[this.addtlPairs.length - 1].$name
        .parent().parent().remove();
    this.addtlPairs[this.addtlPairs.length - 1].$value
        .parent().parent().remove();
    this.addtlPairs.pop();

    if (!this.addtlPairs.length) {
        this.$delPairBtn.detach();
    }
};

parts.modules.part_builder._locnVal = function (val) {
    var arr, str, locn;

    if (val) {
        arr = val.split("-");
        locn = {};

        this.$locn1.val(arr[0]);
        if (arr[0] === "O") {
            this.$locn4.val(arr[1]);
        } else {
            this.$locn2.val(arr[1]);
            this.$locn3.val(arr[2]);
            this.$locn4.val(arr[3]);
        }
    } else {
        str = this.$locn1.val() + "-";

        if (str[0] !== "O") {
            str += this.$locn2.val() + "-" + this.$locn3.val() + "-";
        }

        str += this.$locn4.val();
        return str;
    }
};

parts.modules.part_builder._locnReset = function () {
    this.$locn1.val("Choose...");
    this.$locn2.val(0);
    this.$locn3.val(0);
    this.$locn4.val("");
};


parts.modules.part_builder._icAddtlVal = function (val) {
    if (val) {
        this.$icAddtlInfo.val(val[0].name);
    } else {
        return [{name: this.$icAddtlInfo.val()}];
    }
};

parts.modules.part_builder._icAddtlReset = function () {
    this.$icAddtlInfo.val("");
};

parts.modules.part_builder._miVal = function (val) {
    var json;

    if (val) {
        this.$miValue.val(val.value);
        this.$miName.val(val.name);
    } else {
        json = {
            name: this.$miName.val(),
            value: this.$miValue.val()
        };

        if (json.value === "") {
            delete json.value;
        }

        return json;
    }
};

parts.modules.part_builder._miReset = function () {
    this.$miValue.val(0);
    this.$miName.val("");
};

parts.modules.part_builder._partAddtlVal = function (val) {
    var self = this;

    if (val) {
        val.forEach(function (pair) {
            self.newPair(pair);
        });
    } else {
        return this.addtlPairs.map(function (pair) {
            return {
                name: pair.$name.val(),
                value: pair.$value.val()
            };
        });
    }
};

parts.modules.part_builder._partAddtlReset = function () {
    var i, len = this.addtlPairs.length;

    for (i = 0; i < len; i++) {
        this.delPair();
    }
};

parts.modules.part_builder.populate = function (skel) {
    var self = this;

    $.when.apply(null, this.promises).then(function () {
        if (skel.category === "IC") {
            self.icForm.toggleEdit(true);
            self.icForm.populate(skel);
            self.icForm.url = parts.url + "/parts/" + skel.id + "/";
        } else {
            self.partForm.toggleEdit(true);
            self.partForm.populate(skel);
            self.partForm.url = parts.url + "/parts/" + skel.id + "/";
        }
    });
};

parts.modules.part_builder.reset = function () {
    this.icForm.toggleEdit(false);
    this.icForm.reset();
    this.icForm.url = parts.url + "/parts/";
    this.partForm.toggleEdit(false);
    this.partForm.reset();
    this.partForm.url = parts.url + "/parts/";
};
