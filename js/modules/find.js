"use strict";

parts.modules.find = {};

parts.modules.find.setup = function () {
    var self = this;

    this._parts = {};
    this.$frame = $("#find");
    this.$results = this.$frame.find("tbody");
    this.$resultsCnt = this.$frame.find(".badge");
    this.$seeAll = utils.btn("");
    
    this.searchForm = new utils.Form({
        category: utils.Form.gen.select("Category"),
        query: utils.Form.gen.input("Search Term")
    }, {
        url: parts.url + "/parts/search/",
        noReset: true,
        successAction: function (data) {
            self.$results.empty().append(data.parts.map(self._map.bind(self)))
            self.$resultsCnt.text(data.parts.length);
            self.searchForm.$confirm.text("Search");
        },
        failAction: function () {
            self.searchForm.$confirm.text("Search");
        }
    });

    this.$frame.prepend(this.searchForm.render());

    this.searchForm.skel.category.fill(["Loading..."]);
    this.searchForm.$confirm.text("Search").after(this.$seeAll);
    
    this.searchForm.skel.category.change(function () {
        if (this.value !== "Choose...") {
            self.$seeAll.text("See All " + this.value + "S");
        } else {
            self.$seeAll.text("");
        }
    });

    this.$seeAll.click(this._load.bind(this.$seeAll[0], this));
    this.searchForm.$confirm.click(function () {
        self.searchForm.$confirm.html("<img src=\"/img/loading_green.gif\">");
    });
    
    $.get(parts.url + "/parts/category/names", function (data) {
        self.searchForm.skel.category.empty().fill(data.cats, true);
    });
};

parts.modules.find._load = function (find) {
    var cat = find.searchForm.skel.category.val(), dfd = new $.Deferred(),
        url = parts.url + "/parts/", currText = this.innerHTML, self = this;

    this.innerHTML = "<img src=\"/img/loading_blue.gif\">";
    if (!find._parts[cat]) {
        $.get(url, {filter: "category==" + cat}, utils.orAlert(function (data) {
            find._parts[cat] = data.parts;
            dfd.resolve();
        }));
    } else {
        dfd.resolve();
    }

    dfd.done(function () {
        find.$results.empty().append(find._parts[cat].map(find._map.bind(find)));
        self.innerHTML = currText;
        find.$resultsCnt.text(find._parts[cat].length);
    });
};

parts.modules.find._map = function (json) {
    var $btn = utils.btn("Add to Cart"),
        part = new parts.Part(json);

    $btn.click(function () {
        parts.modules.sell.items.push(part);
        parts.modules.sell.show();
        utils.info({msg: "Added to cart.", dismiss: true});
    });

    return utils.toTr(
        json.id,
        part.toString(),
        utils.toMoney(json.price),
        json.location || "<em>Not available</em>",
        $btn
    );
};
