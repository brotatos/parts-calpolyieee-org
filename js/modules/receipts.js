"use strict";

parts.modules.receipts = {};

parts.modules.receipts.setup = function () {
    var self = this;

    this.$frame = $("#receipts");
    this.$receiptIn = this.$frame.find("input");
    this.$receiptBtn = this.$frame.find("a.btn");
    this.$resultsTable = this.$frame.find("tbody");
    this.$resultsCnt = this.$frame.find("span.badge");

    this.$receiptBtn.click(function () {
        var url = parts.url + "/receipts/search/" +
            (self.$receiptIn.val() || " ") + "/";
        
        $.get(url, utils.orAlert(function (data) {
            self.$resultsCnt.text(data.receipts.length);
            self.$resultsTable.empty().append(
                data.receipts.map(self._mapReceipt, self)
            );
        }));
    });

    this.$receiptIn.keyup(function (event) {
        if (event.which === 13) {
            self.$receiptBtn.click();
        }
    });
};

parts.modules.receipts._mapReceipt = function (receipt) {
    return utils.toTr(
        receipt.id,
        receipt.purchaser_name || "<em>No Name Given</em>",
        receipt.items.reduce(function (str, item) {
            return str + "<li>" + new parts.Part(item).renderToFinal().text()
                .substr(0, 50) + "</li>";
        }, "<ul>"),
        utils.toMoney(receipt.total_price),
        "<a class=\"btn btn-danger\" href=\"" + parts.url + "/receipts/pdf/" +
            receipt.id + ".pdf\">Generate PDF</a>"
    );
};
    
