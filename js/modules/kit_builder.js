"use strict";

parts.modules.kit_builder = {};

parts.modules.kit_builder.setup = function () {
    var self = this;

    this._parts = {};
    this.$frame = $("#kitBuilder");
    this.$kitFormWrapper = this.$frame.children("div:nth-child(1)");
    this.$searchFormWrapper = this.$frame.children("div:nth-child(2)");
    this.$seeAll = utils.btn("");
    this.$results = this.$frame.find("div:nth-child(6) tbody");
    this.$resultsCnt = this.$frame.find(".badge");
    this.$addKitBtn = this.$frame.children("#addKitBtn");
    this.$deleteKitBtn = this.$frame.children("a.btn.btn-danger");
    this.$kitList = this.$frame.find("div:nth-child(5) tbody");
    this.kitList = [];

    ///kit form///
    this.kitForm = new utils.Form({
        name: utils.Form.gen.input("Name"),
        price: utils.Form.gen.money("Price"),
        stock: utils.Form.gen.input("Stock"),
        location: utils.Form.gen.input("Location"),
        kit_items: utils.Form.gen.custom(
            $("<div>"),
            this._itemsVal.bind(this),
            this._itemsReset.bind(this)
        )
    }, {
        url: parts.url + "/kits/"
    });

    this.$kitFormWrapper.append(this.kitForm.render());

    ///search form///
    this.searchForm = new utils.Form({
        category: utils.Form.gen.select("Category"),
        query: utils.Form.gen.input("Search Term")
    }, {
        url: parts.url + "/parts/search/",
        noReset: true,
        successAction: function (data, json) {
            self.$results.empty()
                .append(data.parts.map(self._map.bind(self)));
            self.$resultsCnt.text(data.parts.length);
        }
    });

    this.$searchFormWrapper.append(this.searchForm.render());

    this.searchForm.$confirm.text("Search").after(this.$seeAll);

    this.$seeAll.click(parts.modules.find._load.bind(this.$seeAll[0], this));
    this.searchForm.skel.category.change(function () {
        if (this.value !== "Choose...") {
            self.$seeAll.text("See All " + this.value + "S");
        } else {
            self.$seeAll.text("");
        }
    });

    $.get(parts.url + "/parts/category/names/", utils.orAlert(function (data) {
        self.categories = data.cats;
        self.searchForm.skel.category.fill(self.categories, true);
    }));

    this.$addKitBtn.click(this.kitForm._submit.bind(this.kitForm));

    this.$deleteKitBtn.click(utils.doubleCheck("delete this kit?", function () {
        $.delete_(self.kitForm.url, utils.orAlert(function () {
            utils.info("Kit successfully deleted.");
            location.reload();
        }));
    }));
};

parts.modules.kit_builder._map = function (part) {
    var self = this, tmp = new parts.Part(part), $btn = utils.btn("Add");

    $btn.click(function () {
        if (self.kitList.indexOf(tmp) === -1) {
            self.$kitList.append(tmp.renderToLineItem());
            self.kitList.push(tmp);

            tmp.$liRemove.click(function () {
                self.kitList.splice(self.kitList.indexOf(tmp), 1);
                tmp.$liRemove.parent().parent().remove();
            });
        }
    });

    return utils.toTr(tmp.skel.id, tmp + "", $btn);
};

parts.modules.kit_builder.populate = function (skel) {
    this.kitForm.toggleEdit(true);
    this.kitForm.populate(skel);
    this.kitForm.url = parts.url + "/kits/" + skel.id + "/";
    this.$addKitBtn.text("Save Changes")[0].className = "btn btn-block btn-primary";
};

parts.modules.kit_builder.reset = function () {
    this.kitForm.toggleEdit(false);
    this.kitForm.reset();
    this.kitForm.url = parts.url + "/kits/";
    this.$addKitBtn.text("Create Kit")[0].className = "btn btn-block btn-success";
};

parts.modules.kit_builder._itemsVal = function (val) {
    var self = this;

    if (val) {
        this.$kitList.empty();
        this.kitList.length = 0;
        val.forEach(function (part) {
            var tmp = new parts.Part(part);

            self.$kitList.append(tmp.renderToLineItem());
            self.kitList.push(tmp);

            tmp.$liRemove.click(function (event) {
                self.kitList.splice(self.kitList.indexOf(tmp), 1);
                tmp.$liRemove.parent().parent().remove();

                event.preventDefault();
            });
        });
    } else {
        return this.kitList.map(function (item) {
            return item.skel;
        });
    }
};

parts.modules.kit_builder._itemsReset = function () {
    this.$kitList.empty();
    this.kitList.length = 0;
};
