"use strict";

parts.Part = function (skel) {
    var self = this;

    this.skel = skel;
    if (!skel.quantity) {
        this.skel.quantity = 1;
    }
    this.$liQuantity = $("<input type=\"text\">");
    this.$liRemove = $("<i class=\"fa fa-times fa-lg\">");

    this.$liQuantity.keyup(function () {
        self.skel.quantity = parseInt($(this).val(), 10);
    });
};

parts.Part.prototype.renderToKit = function () {
    return $("<div class='item'>").html(this.skel.quantity + " x " + this);
};

parts.Part.prototype.toString = function () {
    return this.skel.category + " " +
        parts.Part.toHuman(this.skel.main_info) + " " +
        this.skel.addtl_info.reduce(function (str, pair) {
            return str + parts.Part.toHuman(pair) + " ";
        }, "") + " " +
        (this.skel.chip_type || "") + " " +
        (this.skel.mftr || "") + " " +
        (this.skel.name || "") + " ";
};

parts.Part.prototype.renderToFinal = function () {
    return $("<div>").append(
        this.skel.quantity + " x " + this + "@ " +
            utils.toMoney(this.skel.price) + "<span> = " +
            utils.toMoney(this.skel.quantity * this.skel.price) + "</span>"
    );
};

parts.Part.prototype.renderToLineItem = function () {
    return utils.toTr(
        this.$liQuantity.val(this.skel.quantity),
        this.skel.id || "<em>n/a</em>",
        this.skel.category,
        parts.Part.toHuman(this.skel.main_info),
        this.skel.price &&
            utils.toMoney(this.skel.price) || "<em>n/a</em>",
        this.$liRemove
    );
};

parts.Part.toSymbol = function (unit) {
    switch (unit) {
    case "Ohm": return "&Omega;";
    case "Farad": return "F";
    case "Watt": return "W";
    case "Hertz": return "Hz";
    case "Volt": return "V";
    case "Henry": return "H";
    case "meter": return "m";
    default: return unit;
    }
};

parts.Part.toHuman = function (pair) {
    //positive magnitude prefixes
    var posMags = ["", "k", "M", "G", "T", "P"];
    //negative magnitude prefixes
    var negMags = ["", "m", "&micro;", "n", "p", "f", "a"];
    var mag = 0, unit, value;

    if(!pair.hasOwnProperty("value")) {
        return pair.name;
    }

    value = pair.value = parseFloat(pair.value);

    if (pair.value && pair.value > 999) {
        while (value > 1000) {
            value /= 1000;
            mag++;
        }
        unit = posMags[mag] + parts.Part.toSymbol(pair.name);
    } else if(pair.value && pair.value < 1) {
        while (value < 1) {
            value *= 1000;
            mag++;
        }
        unit = negMags[mag] + parts.Part.toSymbol(pair.name);
    } else {
        unit = parts.Part.toSymbol(pair.name);
    }

    //the extra '+' before value removes trailing zeros
    return +value.toFixed(2) + unit;
};
